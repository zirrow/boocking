<?php
require_once __DIR__ . '/../config.php';

require_once __DIR__.'/../vendor/autoload.php';

//подключаем  Silex и передаем в него какие-то конфиги
$app = new Silex\Application();

//подключаем графический интерпритатор

$app->register(new Silex\Provider\TwigServiceProvider(), array(
	'twig.path' => DIR_CATALOG.'view/',
));

//инициируем поддержку сесий

$app->register(new Silex\Provider\SessionServiceProvider());

//Инициализируем поддержку отправки писем

$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app['swiftmailer.options'] = array(
	'host' => MAIL_HOST,
	'port' => MAIL_PORT,
	'username' => MAIL_USER,
	'password' => MAIL_PASS,
	'encryption' => null,
	'auth_mode' => null
);


//подключаем и настраиваем базу данных
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
	'db.options' => array (
			'driver'    => 'pdo_mysql',
			'host'      => DB_HOST,
			'dbname'    => DB_NAME,
			'user'      => DB_USER,
			'password'  => DB_PASS,
			'charset'   => 'utf8mb4',
	),
));

//Подключаем логирование
$app->register(new Silex\Provider\MonologServiceProvider(), array(
	'monolog.logfile' => DIR_ROOT.'/log.log',
));

//Пишем роуты и присваиваем им имена.
$app->get('/', 'controller\index::home')->bind('homepage');
$app->post('/ajaxmail.html', 'controller\index::AjaxMail')->bind('ajaxmail');

$app->get('/hotel.html', 'controller\hotel::GetHotelById')->bind('hotel');
$app->get('/card.html', 'controller\card::card')->bind('card');

/* Примеры пост запроса
$app->post('/pay-add.html', 'controller\pay::payAdd')->bind('pay-add');
*/

$app['debug'] = true;

$app->run();