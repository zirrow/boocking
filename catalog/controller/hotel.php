<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class hotel {

	public function GetHotelById(Request $request, Application $app)
	{

		$twig['title'] = '';
		$hotel_id = $request->get('hotel_id');
		$twig = array();
		$program = new \model\hotel;

		if (isset($hotel_id) && $hotel_id != ''){

			$hotel = $program->GetHotelById($hotel_id,$app);

			if(!empty($hotel)){
				$twig['title'] = $hotel[0]['name'];
				$twig['hotel'] = $hotel[0];
				$twig['desc'] = htmlspecialchars_decode($hotel[0]['desc']);
				$twig['images'] = unserialize($hotel[0]['images']);
				$twig['rooms'] = $program->GetRoomsByHotelId($hotel_id,$app);
			} else{
				$twig['error'] = $twig['title'] = 'Such hotel does not exist';
			}

		} else {
			$twig['error'] = $twig['title'] = 'Such hotel does not exist';
		}

		return $app['twig']->render('hotel.twig',$twig);

	}


}