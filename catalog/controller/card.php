<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class card {

	public function card(Request $request, Application $app)
	{
		$twig['title'] = 'Card';
		$twig['hotel_id'] = $request->get('hotel_id');
		$twig['room_id'] = $request->get('room_id');


		return $app['twig']->render('card.twig',$twig);

	}


}