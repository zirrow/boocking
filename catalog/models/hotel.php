<?php
namespace model;

use Silex\Application;

class hotel {

	public function GetHotelById($hotel_id,$app)
	{
		$sql = 'SELECT h.*'
			. ' FROM `hotels` h'
			. ' WHERE `hotel_id` = '.$hotel_id;

		$result = $app['db']->fetchAll($sql);

		return $result;
	}

	public function GetRoomsByHotelId($hotel_id,$app)
	{
		$sql = 'SELECT r.*'
			. ' FROM `rooms` r'
			. ' WHERE `hotel_id` = '.$hotel_id;

		$result = $app['db']->fetchAll($sql);

		return $result;
	}

}