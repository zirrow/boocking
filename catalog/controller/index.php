<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class index {

	public function home(Application $app)
	{
		$program = new \model\index;
		$twig['counsrys'] = $program->GetCountrys($app);
		$twig['hotels'] = $program->GetHotels($app);
		$twig['testimonials'] = $program->GetTestimonials($app);

		return $app['twig']->render('index.twig',$twig);

	}

	public function AjaxMail(Request $request, Application $app)
	{
		$json = '';

		$twig['name']       = $request->get("name");
		$twig['mail']       = $request->get("email");
		$twig['subject']    = $request->get("subject");
		$twig['message']    = $request->get("message");

		if(!isset($twig['name']) || empty($twig['name']) || iconv_strlen($twig['name']) < 2){
			$json['error']['name'] = 'name';
		}

		if(!isset($twig['mail']) || empty($twig['mail']) || !filter_var($twig['mail'], FILTER_VALIDATE_EMAIL)){
			$json['error']['mail'] = 'mail';
		}

		if(!isset($twig['subject']) || empty($twig['subject']) || iconv_strlen($twig['subject']) < 10){
			$json['error']['subject'] = 'subject';
		}

		if(!isset($twig['message']) || empty($twig['message'])){
			$json['error']['message'] = 'message';
		}

		if(!isset($json['error'])){

			$message = \Swift_Message::newInstance()
				->setSubject($twig['subject'])
				->setFrom(array($twig['mail']))
				->setTo(array(MAIL_USER))
				->setBody($app['twig']->render('mail/mail.twig',$twig));

			$app['swiftmailer.use_spool'] = false;
			$app['mailer']->send($message);

			$json['success'] = 'success';
		}

		return $app->json($json, 200);
	}

}