<?php
namespace model;

use Silex\Application;

class index {

	public function GetCountrys($app)
	{
		$sql = 'SELECT *'
			. ' FROM `countrys`';

		$result = $app['db']->fetchAll($sql);

		return $result;
	}

	public function GetHotels($app)
	{
		$sql = 'SELECT h.*,'
			. ' (SELECT MAX(r.price) FROM `rooms` r WHERE r.hotel_id = h.hotel_id) AS max_price,'
			. ' (SELECT MIN(r.price) FROM `rooms` r WHERE r.hotel_id = h.hotel_id) AS min_price'
			. ' FROM `hotels` h';

		$result = $app['db']->fetchAll($sql);

		return $result;
	}

	public function GetTestimonials($app)
	{
		$sql = 'SELECT *'
			. ' FROM `testimonials`';

		$result = $app['db']->fetchAll($sql);

		return $result;
	}

}